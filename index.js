const env = require('./config.js');
const Telegraf = require('telegraf');
const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');
const axios = require('axios');
const bot = new Telegraf(env.token);

const tecladoOpcoes = Markup.keyboard( [
    ['O que são bots?', 'O que verei no curso?'],
    ['Posso mesmo automatizar tarefas?'],
    ['Como comprar o curso?']
]).resize().extra()

const botoes = Extra.markup( Markup.inlineKeyboard([
    Markup.callbackButton('Sim','s'),
    Markup.callbackButton('Não','n')
], {columns: 2}))

const localizacao = Markup.keyboard([
    Markup.locationRequestButton('Clique aqui para enviar a sua localização!')
]).resize().oneTime().extra()

bot.start( async ctx => {
    const nome = ctx.update.message.from.first_name;
    await ctx.replyWithMarkdown(`*Olá, ${nome}!*\nEu sou o ChatBot do curso`)
    await ctx.replyWithPhoto('https://imgix.ranker.com/user_node_img/50060/1001188616/original/bender-turns-into-a-criminal-in-the-first-episode-photo-u1?w=650&q=50&fm=pjpg&fit=crop&crop=faces')
    await ctx.replyWithMarkdown(`_Posso te ajudar em algo?_`, tecladoOpcoes)
})

bot.hears('O que são bots?', ctx => {
    ctx.replyWithMarkdown(`Bots são blá, bla blá...\n_Algo mais?_`, tecladoOpcoes)
})

bot.hears('O que verei no curso?', async ctx => {
    await ctx.replyWithMarkdown('No *curso* ... tb vamos criar *3 projetos*:')
    await ctx.reply('1. Bot que vai gerenciar a sua lista de compras')
    await ctx.reply('2. Bot que vai te permitir cadastrar seus eventos')
    await ctx.reply('3. E você verá como fui feito, isso mesmo, você podera fazer uma cópia de mim')
    await ctx.replyWithMarkdown('\n\n_Algo mais?_', tecladoOpcoes)
})

bot.hears('Posso mesmo automatizar tarefas?', async ctx => {
    await ctx.replyWithMarkdown('Claro que sim! o bot servirá...\nQuer uma palhinha?', botoes)
})

bot.hears('Como comprar o curso?', ctx => {
    ctx.replyWithMarkdown('Que bom... [link](http://lucaskrahl.com)', tecladoOpcoes)
})

bot.action('n', ctx => {
    ctx.reply('Ok, tudo bem', tecladoOpcoes)
})

bot.action('s', async ctx => {
    await ctx.reply('Que leval, tente me enviar a sua localização, ou escreva uma mensagem qualquer...', localizacao)

})

bot.hears(/mensagem qualquer/i, async ctx => {
    await ctx.replyWithMarkdown('Essa piada é velha, tente outra...', tecladoOpcoes);
    await ctx.replyWithPhoto('https://vignette.wikia.nocookie.net/meme/images/8/83/Cazalb%C3%A9_Risada.gif/revision/latest?cb=20170727002623&path-prefix=pt-br')
})


bot.on('text', async ctx => {
    let msg = ctx.message.text;

    msg = msg.split('').reverse().join('')
    await ctx.reply(`A sua mensagem ao contrário é: ${msg}`);
    await ctx.reply('Isso mostra que eu consigo let o que você escreve e processar a sua mensagem', tecladoOpcoes)
})

bot.on('location', async ctx => {
    try{
        const url = 'http://api.openweathermap.org/data/2.5/weather'
        const { latitude: lat, longitude: lon} = ctx.message.location
        //console.log(lat,lon);
        const res = await axios.get(`${url}?lat=${lat}&lon=${lon}&APPID=76756645d7fa2e607f2023068cb0e183&units=metric`)
        await ctx.reply(`Hmm... Você está em ${res.data.name}`);
        await ctx.reply(`A temperatura por aí esta em ${res.data.main.temp}°C`, tecladoOpcoes)
    }catch( e){
        console.log(e);
        ctx.reply('Estou tento problemas para pegar a sua localização... você esta no planeta terra? :p', tecladoOpcoes)
    }
})

bot.launch()